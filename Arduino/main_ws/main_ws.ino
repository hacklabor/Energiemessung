#include <SoftReset.h>
#include <SPI.h>
#include <Ethernet.h>
#include <SD.h>
#include <PubSubClient.h>
#include <EEPROM.h>

/* Eneriemonitor
 Zählen der Pulse von den Energiemessgeräten in der Hauptverteilung(HV) Gesamt
 und Unterverteilung (UV) Küche und Serverschrank
 Anzeige der Messergebnisse als Html WEB Server
 speichern der Werte auf SD Karte alle 10s(Leistung) und alle 10min 60min 24h 7Tage 30 Tage (Energie)
 Backup EEprom der gezählten Pulse alle 500 Pulse 0,5kWh Gesamtenergieverbrauch


Kähler Thomas 23.12.2016

enthält code aus den Standart Beispielen
Web Server, Listfiles, Udp NTP Client,


*/

//Faktoren der Energiemessgeräte
const unsigned int PulseJekWh_Gesamt = 1000;
const unsigned int PulseJekWh_Kueche = 2000;
const unsigned int PulseJekWh_EDV = 2000;
String txt;
File root;
File myFile;
unsigned long start_ms;
//NextTrigger ArayNr 0=Alle 1=10min 2=1h 3=24h 4=7d 5=30d 6=30s
unsigned long NextTrigger[10];
//TriggerTime ArayNr 0=10s 1=10min 2=1h 3=24h 4=7d 5=30d 6=30s
unsigned long TriggerTime[] = {10, 600,3600,86400,604800,2592000,30};
//Zeit Variablen
unsigned int Stunde;
unsigned int Minute;
unsigned int Sekunde;
unsigned long Unix_TS;
unsigned long secsSince1900;
unsigned long Unix_TS_sync;
unsigned long Unix_TS_now;
unsigned long Unix_TS_start;
//Pulse ArayNr 0=Alle 1=10min 2=1h 3=24h 4=7d 5=30d
unsigned long PulseS0_Gesamt[10];
unsigned long PulseS0_Kueche[10];
unsigned long PulseS0_EDV[10];
//LetztePulse Merker ArayNr 0=Alle 1=10min 2=1h 3=24h 4=7d 5=30d
unsigned long PulseS0_alt_Gesamt[10];
unsigned long PulseS0_alt_Kueche[10];
unsigned long PulseS0_alt_EDV[10];
//Zeit Zwischen den letzten 2 Pulsen solange Zeit seit letzen Puls < sonst Zeit seit letzten Puls
//Berechnung Leistung
unsigned long TimeS0_Gesamt;
unsigned long TimeS0_Kueche;
unsigned long TimeS0_EDV;
//Zeit Zwischen den letzten 2 Pulsen
//Berechnung Leistung
unsigned long ValidTimeS0_Gesamt;
unsigned long ValidTimeS0_Kueche;
unsigned long ValidTimeS0_EDV;
//Zeitmerker Letzter PULS
unsigned long LastTimeS0_Gesamt;
unsigned long LastTimeS0_Kueche;
unsigned long LastTimeS0_EDV;
//Merker ab welcher Pulszahl der Pulswert im EEprom gespeichert wird
unsigned long EEpromUpdate;
//Merker Zeitpunkt der UNIX TS Aktualisierung Wichtig f�r �berlauf der LONG Millis() nach ca 50 Tagen
unsigned long millis_old;
// Zeitpunkt Millis() der letzten NTC Aktualisierung berechnen der Aktuellen Unix Zeit (Unix_TS_now=
unsigned long TimeOffset;
char Buffer[10];
// EEprom Adressen um Pulse zu speichern
int EEprom_Gesamt = 0;
int EEprom_Kueche = 10;
int EEprom_EDV = 20;
// Adresen der LED 
byte LED_allgmein = 13;
byte LED_INTERUPT = 12;

boolean   INT_STATE;
// Namen der Datein zur speicherung auf SD
String FileName[] = { "Pulse10s.csv","10min.csv","1h.csv","24h.csv","7d.csv","30d.csv" };
String MqttName[] = { "Pulse","Energie10min","Energie1h","Energie24h","Energie7d","Energie30d" };


// Enter a MAC address and IP address for your controller below.
// The IP address will be dependent on your local network:
byte mac[] = {
	0xDE, 0xAD, 0xBE, 0x44, 0x44, 0x44
};
// Not USE da DHCP
IPAddress ip(192, 168, 178, 97);

// MQTT Adresse Server
//const char* mqtt_server = "10.42.255.248";
const char* mqtt_server = "hautomation01.hacklabor.de";
EthernetClient EthClient;
PubSubClient MQTTclient(EthClient);

// Initialize the Ethernet server library
// with the IP address and port you want to use
// (port 80 is default for HTTP):
EthernetServer server(80);
//
//NTP Server init Variablen
unsigned int localPort = 8888;       // local port to listen for UDP packets
char timeServer[] = "ptbtime1.ptb.de"; // time.nist.gov NTP server
const int NTP_PACKET_SIZE = 48; // NTP time stamp is in the first 48 bytes of the message
byte packetBuffer[NTP_PACKET_SIZE]; //buffer to hold incoming and outgoing packets
EthernetUDP Udp;						// A UDP instance to let us send and receive packets over UDP




void setup() {
	
	pinMode(LED_INTERUPT, OUTPUT);
	pinMode(LED_allgmein, OUTPUT);
	//Interupt Aktiv setzen
	pinMode(2, INPUT_PULLUP);
	attachInterrupt(digitalPinToInterrupt(2), Interupt_S0_HV, RISING);
	pinMode(18, INPUT_PULLUP);
	attachInterrupt(digitalPinToInterrupt(18), Interupt_S0_kueche, RISING);
	pinMode(19, INPUT_PULLUP);
	attachInterrupt(digitalPinToInterrupt(19), Interupt_S0_EDV, RISING);
	

	// Open serial communications and wait for port to open:
		Serial.begin(9600);
	while (!Serial) {
		; // wait for serial port to connect. Needed for native USB port only
	}

	//EEPROM letzte PULSE Daten aus EEprom laden
	PulseS0_Gesamt[0] = EEPROMReadlong(EEprom_Gesamt);
	PulseS0_Kueche[0] = EEPROMReadlong(EEprom_Kueche);
	PulseS0_EDV[0] = EEPROMReadlong(EEprom_EDV);
	for (int i = 0; i < 10; i++) {
		PulseS0_alt_Gesamt[i] = PulseS0_Gesamt[0];
		PulseS0_alt_Kueche[i] = PulseS0_Kueche[0];
		PulseS0_alt_EDV[i] = PulseS0_EDV[0];
	}
	// Start und Pr�fen ob SD Karte vorhanden
	if (!SD.begin(4)) {
		Serial.println("initialization failed!");
		return;
	}
	Serial.println("initialization done.");
	
	// SD Files Init (schreiben der Header)
	String tmp = "Starts.csv";
	if (SD.exists(tmp) == false) {
		LogFileWrite(tmp, "Last_Starttime");
	}
	 tmp = "Leistung.csv";
	if (SD.exists(tmp) == false) {
		LogFileWrite(tmp, "Timstamp;Pulse_Gesamt;P_Gesamt;Pulse_Kueche;P_Kueche;Pulse_EDV;P_EDV;Diff_Pulse;Diff_P");
	}
	
	
	for (int i=1; i < 6; i++) {
		tmp = FileName[i];
		if (SD.exists(tmp) == false) {
			LogFileWrite(tmp, "Time;Pulse_Gesamt;E_HV;Pulse_Kueche;E_Kueche;Pulse_EDV;E_EDV;Diff_Pulse;Diff_Energie");
		}
	}

	// MQTT Start
	

	// start the Ethernet connection and the server:
	Ethernet.begin(mac); // hier kann man auch die Statische IP nutzen Ethernet.begin(mac,ip);
	server.begin();
	Serial.print("server is at ");
	Serial.println(Ethernet.localIP());
	Udp.begin(localPort);

	// Aktuelle Zeit �ber Ntc Server holen
	if (Unix_TS_start == 0) { GetNtc(); }
	  Unix_TS_start = Unix_TS_sync;
	  if (Unix_TS_start == 0) {GetNtc(); }
	  Unix_TS_start = Unix_TS_sync;

		LogFileWrite("Starts.csv", String(Unix_TS_sync));
		TimeNow();
		for (int i = 0; i < 7; i++) {
			
			NextTrigger[i] = Unix_TS_now + TriggerTime[i];
		}

		MQTTclient.setServer(mqtt_server, 1883);
	// INIT DONE Gr�ne LED EIN
	digitalWrite(LED_allgmein, HIGH);
	
}

//*****************Beginn Interuppt Routinen************************
void Interupt_S0_HV() {
	PulseS0_Gesamt[0] += 1;
	unsigned long tmp = millis();
	TimeS0_Gesamt = tmp - LastTimeS0_Gesamt;
	ValidTimeS0_Gesamt = TimeS0_Gesamt;
	LastTimeS0_Gesamt = tmp;
	// Toogle gelbe LED 
		INT_STATE = !INT_STATE;
}

void Interupt_S0_kueche() {
	PulseS0_Kueche[0] += 1;
	unsigned long tmp = millis();
	TimeS0_Kueche = tmp - LastTimeS0_Kueche;
	ValidTimeS0_Kueche = TimeS0_Kueche;
	LastTimeS0_Kueche = tmp;
	// Toogle gelbe LED 
	INT_STATE = !INT_STATE;
}

void Interupt_S0_EDV() {
	PulseS0_EDV[0] += 1;
	unsigned long tmp = millis();
    TimeS0_EDV= tmp - LastTimeS0_EDV;
	ValidTimeS0_EDV = TimeS0_EDV;
	LastTimeS0_EDV = tmp;
	// Toogle gelbe LED 
	INT_STATE = !INT_STATE;
}
//***************** ENDE Interuppt Routinen ************************




//***************** Begin Mqtt Funktionen ************************
void reconnect()
{

	
	
		Serial.print("Attempting MQTT connection...");
		// Attempt to connect
		if (MQTTclient.connect("Energie"))
		{
			Serial.println("connected");
		}
		else
		{
			Serial.print("failed, rc=");
			Serial.print(MQTTclient.state());
					}
	}

void MqttWrite(String Name, char *data) {
	char *buf;

	unsigned int  size = Name.length();
	Name.toCharArray(buf, size);
	MQTTclient.publish(buf, data);

}

char *leadingZero(char *input) {
	for (int i = 0; i < 10; i++) {

		if (input[i] == ' ') {
			input[i] = 0;
		}
		 
	}

	return input;
}
//***************** ENDE Mqtt Funktionen ************************

//*****************Begin NTC Timserver Funktionen************************
void sendNTPpacket(char* address) {
	// set all bytes in the buffer to 0
	memset(packetBuffer, 0, NTP_PACKET_SIZE);
	// Initialize values needed to form NTP request
	// (see URL above for details on the packets)
	packetBuffer[0] = 0b11100011;   // LI, Version, Mode
	packetBuffer[1] = 0;     // Stratum, or type of clock
	packetBuffer[2] = 6;     // Polling Interval
	packetBuffer[3] = 0xEC;  // Peer Clock Precision
							 // 8 bytes of zero for Root Delay & Root Dispersion
	packetBuffer[12] = 49;
	packetBuffer[13] = 0x4E;
	packetBuffer[14] = 49;
	packetBuffer[15] = 52;

	// all NTP fields have been given values, now
	// you can send a packet requesting a timestamp:
	Udp.beginPacket(address, 123); //NTP requests are to port 123
	Udp.write(packetBuffer, NTP_PACKET_SIZE);
	Udp.endPacket();
}



void GetNtc() {
	sendNTPpacket(timeServer); // send an NTP packet to a time server

							   // wait to see if a reply is available
	delay(2000);
	if (Udp.parsePacket()) {
		// We've received a packet, read the data from it
		Udp.read(packetBuffer, NTP_PACKET_SIZE); // read the packet into the buffer

												 // the timestamp starts at byte 40 of the received packet and is four bytes,
												 // or two words, long. First, extract the two words:

		unsigned long highWord = word(packetBuffer[40], packetBuffer[41]);
		unsigned long lowWord = word(packetBuffer[42], packetBuffer[43]);
		// combine the four bytes (two words) into a long integer
		// this is NTP time (seconds since Jan 1 1900):
		secsSince1900 = highWord << 16 | lowWord;
		Serial.print("Seconds since Jan 1 1900 = ");
		Serial.println(secsSince1900);

		// now convert NTP time into everyday time:
		Serial.print("Unix time = ");
		// Unix time starts on Jan 1 1970. In seconds, that's 2208988800:
		const unsigned long seventyYears = 2208988800UL;
		start_ms = millis();
		TimeOffset = 0;

		// subtract seventy years:
		unsigned long epoch = secsSince1900 - seventyYears;
		
		Unix_TS_sync = epoch;
		// print Unix time:
		Serial.println(epoch);
		Serial.println(start_ms);

		// print the hour, minute and second:
		Serial.print("The UTC time is ");       // UTC is the time at Greenwich Meridian (GMT)
		Serial.print((epoch % 86400L) / 3600); // print the hour (86400 equals secs per day)
		Serial.print(':');
		if (((epoch % 3600) / 60) < 10) {
			// In the first 10 minutes of each hour, we'll want a leading '0'
			Serial.print('0');
		}
		Serial.print((epoch % 3600) / 60); // print the minute (3600 equals secs per minute)
		Serial.print(':');
		if ((epoch % 60) < 10) {
			// In the first 10 seconds of each minute, we'll want a leading '0'
			Serial.print('0');
		}
		Serial.println(epoch % 60); // print the second
	}
	// wait ten seconds before asking for the time again
}
//*****************ENDE NTC Timserver Funktionen************************

//*****************Begin ZEIT Funktionen************************


void TimeNow() {
	// �berlauf Millis bei 4294967296ms Abfangen
	if (millis() < millis_old) {
	TimeOffset += 4294967;
	}
	Unix_TS_now = Unix_TS_sync+ TimeOffset + (millis() / 1000) - (start_ms / 1000);
	millis_old = millis();
	ZeitUnix(Unix_TS_now);
}

void ZeitUnix(unsigned long UnixT) {


	
	
	
		Stunde = (UnixT % 86400L) / 3600;
		Minute = (UnixT % 3600) / 60;
		Sekunde = UnixT % 60;
	
	


}

void ZeitMillis(unsigned long Time) {
//Zerlegen Unix Timestamp in Stunde, Minute und Sekunde
	Time = Time / 1000;
	Serial.println(Time);



	Stunde = (Time / 3600);
	Minute = (Time % 3600) / 60;
	Sekunde = Time % 60;
	Serial.println(Stunde);
	Serial.println(Minute);
	Serial.println(Sekunde);

}
//*****************Ende ZEIT Funktionen************************


//*****************Begin WEB Server Funktionen************************
void ListFiles(EthernetClient client) {
// Anzeigen aller Files die auf der SD Karte vorhanden sind
	Serial.println("List Files");
	root = SD.open("/");
	root.rewindDirectory();
	client.println("<ul>");
	while (true) {
		Serial.println("FileOK");
		File entry = root.openNextFile();
		if (!entry) {
			// no more files
			Serial.println("FileBreak");
			break;
		}
		Serial.println("FileOK1");
		if (!entry.isDirectory()) {
			client.print("<li><a href=\"");
			client.print(entry.name());
			Serial.print(entry.name());
			client.print("\"");
			client.print(">");
			client.print(entry.name());
			client.print("</a>");
			client.println("</li>");
		}
		entry.close();

	}
	client.println("</ul>");
	root.close();
}

void PrintZeit(EthernetClient client) {
  // Formatierung der Zeitausgabe mit führenden Nullen
	if (Stunde < 10) { client.print("0"); }
	client.print(Stunde);
	client.print(":");
	if (Minute < 10) { client.print("0"); }
	client.print(Minute);
	client.print(":");
	if (Sekunde < 10) { client.print("0"); }
	client.println(Sekunde);
}

void PrintEnergie(EthernetClient client, String Ueberschrift,unsigned long Pulse[],unsigned long Pulstime,unsigned long ValidPulstime, unsigned int PulseJekWh) {
	//Anzeige der Energie und Leistungsdaten
  
  //tmpd Gesamt verbrauchte Energie
	double tmpd = (double(Pulse[0])/ double (PulseJekWh));
  //tmpd1 Leistung seit letzten Puls solange diese Zeit Größer als  die Zeit zwischen den letzten beiden Pulsen ist
	double tmpd1 = double( 3600000000)/((double(Pulstime) * double(PulseJekWh)));
  //tmpd2 Leistung aufgrund der Zeitdifferenz der letzten zwei Pulse
	double tmpd2 = double(3600000000) / ((double(ValidPulstime) * double(PulseJekWh)));
	client.print("<h2>"+Ueberschrift+" : "+tmpd+" kWh "+tmpd1 + " W (valid " + tmpd2 + " W)" + "</h2>");
	client.println("<BR>");
	client.print("Pulse: ");
	client.print(Pulse[0]);
	client.print(" | Zeit seit letzten Puls: ");
	tmpd = double(double(Pulstime) / double(1000));
	client.print(tmpd);
	client.print("s | Valid PulsZeit: ");
	tmpd = double(double(ValidPulstime) / double(1000));
	client.print(tmpd);
	client.print("s");
	client.println("<BR>");
	client.println("<BR>");
	 tmpd = (double(Pulse[1]) / double(PulseJekWh));
	 client.print("letzte 10min: ");
	 client.print(tmpd);
	 tmpd = (double(Pulse[2]) / double(PulseJekWh));
	 client.print(" kWh | letzte 1h: ");
	 client.print(tmpd);
	 tmpd = (double(Pulse[3]) / double(PulseJekWh));
	 client.print(" kWh | letzte 24h: ");
	 client.print(tmpd);
	 tmpd = (double(Pulse[4]) / double(PulseJekWh));
	 client.print(" kWh | letzten 7d: ");
	 client.print(tmpd);
	 tmpd = (double(Pulse[5]) / double(PulseJekWh));
	 client.print(" kWh | letzten 30d: ");
	 client.print(tmpd);
	 client.print(" kWh");
	client.println("<BR>");
}

void serverHtml() {
  #define BUFS 100
	char clientline[BUFS];
	int index = 0;

	EthernetClient client1 = server.available();
	if (client1) {
		// an http request ends with a blank line
		boolean current_line_is_blank = true;

		// reset the input buffer
		index = 0;

		while (client1.connected()) {
			if (client1.available()) {
				char c = client1.read();

				// If it isn't a new line, add the character to the buffer
				if (c != '\n' && c != '\r') {
					clientline[index] = c;
					index++;
					// are we too big for the buffer? start tossing out data
					if (index >= BUFS)
						index = BUFS - 1;

					// continue to read more data!
					continue;
				}

				// got a \n or \r new line, which means the string is done
				clientline[index] = 0;

				// Print it out for debugging
				Serial.println(clientline);

				// Look for substring such as a request to get the root file
				if (strstr(clientline, "GET / ") != 0) {
					// send a standard http response header
					client1.println("HTTP/1.1 200 OK");
					client1.println("Content-Type: text/html");
					client1.println("Connection: close");  // the connection will be closed after completion of the response
					client1.println("Refresh: 30");  // refresh the page automatically every 30 sec
					client1.println();
					client1.println("<!DOCTYPE HTML>");
					client1.println("<html>");

					// print all the files, use a helper to keep it clean
					client1.println("<h1>HAL Webserver Energiemessung</h1>");
					client1.print("UNIX TIME = ");

					TimeNow();
					client1.println(Unix_TS_now);
					client1.print(" |  aktuelle Zeit = ");
					PrintZeit(client1);
					client1.println(" |  Zeit Seit Start: ");
					ZeitMillis(millis());
					PrintZeit(client1);
					client1.println("<BR>");
					TimeS0_Gesamt = PulseZeitKorrektur(TimeS0_Gesamt, LastTimeS0_Gesamt);
					PrintEnergie(client1, "Alles", PulseS0_Gesamt, TimeS0_Gesamt, ValidTimeS0_Gesamt, PulseJekWh_Gesamt);
					TimeS0_Kueche = PulseZeitKorrektur(TimeS0_Kueche, LastTimeS0_Kueche);
					PrintEnergie(client1, "Kueche", PulseS0_Kueche, TimeS0_Kueche, ValidTimeS0_Kueche, PulseJekWh_Kueche);
					TimeS0_EDV = PulseZeitKorrektur(TimeS0_EDV, LastTimeS0_EDV);
					PrintEnergie(client1, "EDV", PulseS0_EDV, TimeS0_EDV, ValidTimeS0_EDV, PulseJekWh_EDV);

					client1.println("<h2>Files:</h2>");
					ListFiles(client1);
				}
				else if (strstr(clientline, "GET /") != 0) {
					// this time no space after the /, so a sub-file!
					char *filename;

					filename = clientline + 5; // look after the "GET /" (5 chars)
											   // a little trick, look for the " HTTP/1.1" string and 
											   // turn the first character of the substring into a 0 to clear it out.
					(strstr(clientline, " HTTP"))[0] = 0;

					// print the file we want
					Serial.println(filename);
					myFile = SD.open(filename);
					if (!myFile) {
						client1.println("HTTP/1.1 404 Not Found");
						client1.println("Content-Type: text/html");
						client1.println();
						client1.println("<h2>File Not Found!</h2>");
						break;
					}

					Serial.println("Opened!");

					client1.println("HTTP/1.1 200 OK");
					client1.println("Content-Type: text/plain");
					client1.println();

					int16_t c;
					while ((c = myFile.read()) > 0) {
						// uncomment the serial to debug (slow!)
						//Serial.print((char)c);
						client1.print((char)c);
					}
					myFile.close();
				}
				else {
					// everything else is a 404
					client1.println("HTTP/1.1 404 Not Found");
					client1.println("Content-Type: text/html");
					client1.println();
					client1.println("<h2>File Not Found!</h2>");
				}
				break;
			}
		}
		// give the web browser time to receive the data
		delay(1);
		client1.stop();
	}
}



//*****************Ende WEB Server Funktionen************************

//*****************Begin SD DATA Funktionen************************
void LogLeistung() {
  
  
  if (Unix_TS_now > 1482225220) {
  double Gesamt =double(3600000000) / ((double(TimeS0_Gesamt) * double(PulseJekWh_Gesamt)));
  double Kueche =double(3600000000) / ((double(TimeS0_Kueche) * double(PulseJekWh_Kueche)));
  double EDV = double(3600000000) / ((double(TimeS0_EDV) * double(PulseJekWh_EDV)));
	String data = "";
	data += String(Unix_TS_now);
	data += ";";
	data += String(PulseS0_Gesamt[0]);
	data += ";";
	if (TimeS0_Gesamt == 0) {
		data += "0"; }
	else { 
		data += String(Gesamt);
	}
	data += ";";
	data += String(PulseS0_Kueche[0]);
	data += ";";

	if (TimeS0_Kueche == 0) { 
		data += "0"; }
	else {
		data += String(Kueche);
	}
		
	data += ";";
	data += String(PulseS0_EDV[0]);
	data += ";";
	if (TimeS0_EDV == 0) {
		data += "0"; }
	else {
		data += String(EDV); 
	}
    data += ";";
    data +=String ((PulseS0_Gesamt[0]*2)-PulseS0_Kueche[0]-PulseS0_EDV[0]);
    data += ";";
    if (double(double(Gesamt)-double(Kueche)-double(EDV))>0){
    data +=String (double(double(Gesamt)-double(Kueche)-double(EDV)));
    }
    else{
    data += "0";
    }

	LogFileWrite("Leistung.csv", data);
	if (!MQTTclient.connected()) {
		reconnect();
	}
	MQTTclient.loop();
	
	
	dtostrf(double((double(PulseS0_Gesamt[0]) / double(PulseJekWh_Gesamt))), 1, 2, Buffer);
	MQTTclient.publish("arduino01/mainpower/counter",Buffer);
	dtostrf(double((double(PulseS0_Kueche[0]) / double(PulseJekWh_Kueche))),1, 2, Buffer);
	MQTTclient.publish("arduino01/kitchenpower/counter", Buffer);
	dtostrf(double((double(PulseS0_EDV[0]) / double(PulseJekWh_EDV))), 1, 2, Buffer);
	MQTTclient.publish("arduino01/itpower/counter", Buffer);
	dtostrf(double(Gesamt), 1, 2, Buffer);
	MQTTclient.publish("arduino01/mainpower/current", Buffer);
	dtostrf(double(Kueche), 1, 2, Buffer);
	MQTTclient.publish("arduino01/kitchenpower/current", Buffer);
	dtostrf(double(EDV), 1, 2, Buffer);
	MQTTclient.publish("arduino01/itpower/current", Buffer);
	dtostrf(double(double(Gesamt) - double(Kueche) - double(EDV)), 1, 2, Buffer);
	MQTTclient.publish("arduino01/RestPower/current", Buffer);
    }
	}
void LogPulse() {
	if (Unix_TS_now > 1482225220) {
		String data = "";
		data += String(Unix_TS_now);
		data += ";";
		data += PulseS0_Gesamt[0];
		data += ";";
		data += PulseS0_Kueche[0];
		data += ";";
		data += PulseS0_EDV[0];

		LogFileWrite(FileName[0], data);
	}
}


void LogEnergie(byte arryNr, boolean Mqtt) {
	if (Unix_TS_now > 1482225220) {
		String Name;
		String data = "";
		data += String(Unix_TS_now);
		data += ";";
    data += String(PulseS0_Gesamt[0]);
    data += ";";
		double Gesamt = (double(PulseS0_Gesamt[arryNr]) / double(PulseJekWh_Gesamt));
		data += String(Gesamt);
		data += ";";
    data += String(PulseS0_Kueche[0]);
    data += ";";
		double Kueche = (double(PulseS0_Kueche[arryNr]) / double(PulseJekWh_Kueche));
		data += String(Kueche);
		data += ";";
    data += String(PulseS0_EDV[0]);
    data += ";";
		double EDV = (double(PulseS0_EDV[arryNr]) / double(PulseJekWh_EDV));
		data += String(EDV);
    data += ";";
    data +=String ((PulseS0_Gesamt[0]*2)-PulseS0_Kueche[0]-PulseS0_EDV[0]);
     data += ";";
    data +=String (double(Gesamt)-double(Kueche)-double(EDV));
		LogFileWrite(FileName[arryNr], data);

		//if (Mqtt) {
			//An Mqtt senden
			
	//	}

	}
	
	
	
	
}




void LogFileWrite(String Filename, String dataString) {
	
	File dataFile = SD.open(Filename, FILE_WRITE);

	// if the file is available, write to it:
	if (dataFile) {
		dataFile.println(dataString);
		dataFile.close();
		// print to the serial port too:
		Serial.println(dataString);
	}
	// if the file isn't open, pop up an error:
	else {
		Serial.println("error opening datalog.txt");
	}

}
// send an NTP request to the time server at the given address

//*****************ENDE SD DATA Funktionen************************

//*****************Begin Allgemeine Funktionen************************

long PulseZeitKorrektur(long timeDiff, long lasttime) {
	// Leistungsberechnung Korrektur der Zeitspanne zwischen den Pulsen
	// 
	unsigned long tmp = millis() - lasttime;
	if (tmp > timeDiff) {
		//Zeit seit letzen Puls wenn die Gr��er als die Zeitdiff der letzten zwei Pulse
		return tmp;
	}
	return timeDiff;
}
void GetPulse( byte ArayNr, boolean Mqtt) {
	// Pulse seit letzten AbfrageIntervall (ArayNR= Triggertime intervall) ermitteln 
	PulseS0_Gesamt[ArayNr] = PulseS0_Gesamt[0] - PulseS0_alt_Gesamt[ArayNr];
	 PulseS0_alt_Gesamt[ArayNr]= PulseS0_Gesamt[0];
	 PulseS0_Kueche[ArayNr] = PulseS0_Kueche[0] - PulseS0_alt_Kueche[ArayNr];
	 PulseS0_alt_Kueche[ArayNr] = PulseS0_Kueche[0];
	 PulseS0_EDV[ArayNr] = PulseS0_EDV[0] - PulseS0_alt_EDV[ArayNr];
	 PulseS0_alt_EDV[ArayNr] = PulseS0_EDV[0];
	 LogEnergie(ArayNr,Mqtt);

}
void EEPROMWritelong(int address, long value)
{
	//Decomposition from a long to 4 bytes by using bitshift.
	//One = Most significant -> Four = Least significant byte
	byte four = (value & 0xFF);
	byte three = ((value >> 8) & 0xFF);
	byte two = ((value >> 16) & 0xFF);
	byte one = ((value >> 24) & 0xFF);

	//Write the 4 bytes into the eeprom memory.
	EEPROM.write(address, four);
	EEPROM.write(address + 1, three);
	EEPROM.write(address + 2, two);
	EEPROM.write(address + 3, one);
}
long EEPROMReadlong(int address)
{
	//Read the 4 bytes from the eeprom memory.
	long four = EEPROM.read(address);
	long three = EEPROM.read(address + 1);
	long two = EEPROM.read(address + 2);
	long one = EEPROM.read(address + 3);

	//Return the recomposed long by using bitshift.
	return ((four << 0) & 0xFF) + ((three << 8) & 0xFFFF) + ((two << 16) & 0xFFFFFF) + ((one << 24) & 0xFFFFFFFF);
}

//***************** ENDE Allgemeine Funktionen************************

void loop() {
	byte	trigger = 0;
	// Aktulle UNIX Zeit bestimmen
	TimeNow();

	// alle 500 Pulse HV Pulsewerte in EEprom speichern
	// Zeit bis Eeprom defekt ca 8 Jahre bei 500kWh monatlich
	if (PulseS0_Gesamt[0] > EEpromUpdate) {
		Serial.println("EEPROM_WRITE");
		EEPROMWritelong(EEprom_Gesamt, PulseS0_Gesamt[0]);
		EEPROMWritelong(EEprom_Kueche, PulseS0_Kueche[0]);
		EEPROMWritelong(EEprom_EDV, PulseS0_EDV[0]);
		EEpromUpdate = PulseS0_Gesamt[0] + 500;
		Serial.println(EEpromUpdate);
	}

		trigger = 0; // jede 10Sec

		if(Unix_TS_now >= NextTrigger[trigger]) {
		Serial.print(Unix_TS_now);
			Serial.println("Trigger"+String(trigger));
			// was soll gemacht werden
		TimeS0_Gesamt = PulseZeitKorrektur(TimeS0_Gesamt, LastTimeS0_Gesamt);
		TimeS0_Kueche = PulseZeitKorrektur(TimeS0_Kueche, LastTimeS0_Kueche);
		TimeS0_EDV = PulseZeitKorrektur(TimeS0_EDV, LastTimeS0_EDV);
		

		LogLeistung();
		NextTrigger[trigger] = Unix_TS_now + TriggerTime[trigger];
			}

		trigger = 1; //alle 10min
		if (Unix_TS_now >= NextTrigger[trigger]) {
			Serial.print(Unix_TS_now);
			Serial.println("Trigger" + String(trigger));
			// was soll gemacht werden
			
			GetPulse(trigger,true);
			GetNtc();
			TimeNow();
			NextTrigger[trigger] = Unix_TS_now + TriggerTime[trigger];
		}
		trigger = 2; //alle 60min
		if (Unix_TS_now >= NextTrigger[trigger]) {
			
			Serial.print(Unix_TS_now);
			Serial.println("Trigger" + String(trigger));
			// was soll gemacht werden
			
			
			GetPulse(trigger,true);
			NextTrigger[trigger] = Unix_TS_now + TriggerTime[trigger];
		}
		trigger = 3; //alle 24h
		if (Unix_TS_now >= NextTrigger[trigger]) {
						Serial.print(Unix_TS_now);
			Serial.println("Trigger" + String(trigger));
			// was soll gemacht werden
			GetPulse(trigger,false);
			NextTrigger[trigger] = Unix_TS_now + TriggerTime[trigger];
		}
		trigger = 4; //alle 7d
		if (Unix_TS_now >= NextTrigger[trigger]) {
			
			Serial.print(Unix_TS_now);
			Serial.println("Trigger" + String(trigger));
			// was soll gemacht werden
			GetPulse(trigger,false);
						NextTrigger[trigger] = Unix_TS_now + TriggerTime[trigger];
		}
		trigger = 5; //alle 30d

		if (Unix_TS_now >= NextTrigger[trigger]) {
			
			
			Serial.print(Unix_TS_now);
			Serial.println("Trigger" + String(trigger));
			// was soll gemacht werden
			GetPulse(trigger,false);
			NextTrigger[trigger] = Unix_TS_now + TriggerTime[trigger];
		}
		trigger = 6; // alle 1s

		if (Unix_TS_now >= NextTrigger[trigger]) {
			Serial.print(Unix_TS_now);
			Serial.println("Trigger" + String(trigger));
			// was soll gemacht werden
			
			
			
			NextTrigger[trigger] = Unix_TS_now + TriggerTime[trigger];
		}
		
// Anzeige Interupt Toogle bei jeden Puls
		if (INT_STATE) {
			digitalWrite(LED_INTERUPT, HIGH);
			}
		else {
			digitalWrite(LED_INTERUPT, LOW);
		}
// mach server Voodoo
	serverHtml();

	
}
