# OLD REPO
[New energy-smart-meter-v2.0](https://gitlab.com/hacklabor/energy-smart-meter-v2.0)






# Energiemessung HAL

## installierte Messgeräte

Hauptverteilung Messen des gesamt Energieverbrauchs

[DRT751DE MID M16 - zugelassen bis 2024 Handbuch](http://www.bg-etech.de/download/manual/DRT751DE.pdf) 
 
Unterverteilung 

* Messgerät 1 Serverschrank
* Messgerät 2 Küche
* weitere?
 
[DRS155DC-V3 Handbuch](http://bg-etech.de/download/manual/DRS155DCV3.pdf)

## Meßart

die Messgeräte sind fest in der Haupt bzw Unterververteilung eingebaut

* lmpulsausgang S0 nach DIN EN 62053-31 - Kl. A
* potenzialfrei durch einen Optokoppler,
* max. 27V DC / 20mA

## Auflösung

* 1Wh/Pulse (DRT751DE)
* 0,5Wh/Pulse (DRS155DC-V3)

## Auswerteelektronik

### Arduino Mega
* [Atmega 2560 Handbuch](http://www.atmel.com/images/atmel-2549-8-bit-avr-microcontroller-atmega640-1280-1281-2560-2561_datasheet.pdf) 
* 

### W5100 Ethernet Shield
* https://www.adafruit.com/product/201
* https://github.com/adafruit/SDWebBrowse/blob/master/SDWebBrowse.ino

### RTC
* http://tronixstuff.com/2014/12/01/tutorial-using-ds1307-and-ds3231-real-time-clock-modules-with-arduino/

### Step Up Wandler
* https://www.amazon.de/gp/product/B01E086QZ0/ref=oh_aui_detailpage_o00_s03?ie=UTF8&psc=1



### Layout
![Platine](/Fritzing/Entwurf1_Steckplatine.png  "Erster Entwurf")

## Formeln
### Leistung
P=U*I= W[Ws]/t[s] = 1Pulse/(t<sub>2</sub>-t<sub>1</sub>)
### Unix Timestamp in Date Open Office
=(("UNIXTimstampUTC"+3600)/86400)+25569

![100Pulse](/Rechnungen/UnixTS.PNG)
[/Rechnungen/UnixTS.ods](/Rechnungen/UnixTs.ods "Tool")

### Min Zeit zwischen zwei Pulsen bei 2000Pulse/kWh

P<sub>max</sub>= U*I =230Vx16A=3680W = 7300Pulse
7300Pulse/3600=2,04Pulse/s=0,489s/Pulse

Wertetabelle

![100Pulse](/Rechnungen/Wertetabelle1000PulsejekWh.PNG)
![200Pulse](/Rechnungen/Wertetabelle2000PulsejekWh.PNG)
[/Rechnungen/Rechentool.ods](/Rechnungen/Rechentool.ods "Tool")

## Datenauswertung
### Leistung.CSV

* Datenformat DS getrennt mit Symikolion
* Spalte 1 Timestamp UNIX UTC
* Spalte 2 Aktuelle Pulse HV Gesamt Zähler
* Spalte 3 Leistung UV Kueche (siehe Berechnung)
* Spalte 4 Aktuelle Pulse UV Kueche Zähler
* Spalte 5 Leistung UV EDV (siehe Berechnung)
* Spalte 6 Aktuelle Pulse UV EDV Zähler
* Spalte 7 Leistung HV Gesamt (siehe Berechnung)

#### Berechnung

